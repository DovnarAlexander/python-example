#!env python3
'''
This server changes its behavior upon environment variable change
'''
import os

from flask import Flask
from flask.templating import render_template

app = Flask(__name__, template_folder="templates")


@app.route("/")
def main():
    '''
    Route describing default path handling for the server
    '''
    return render_template(
        "index.html.j2", color=os.getenv("BACKGROUND_COLOR", "green")
    )


if __name__ == "__main__":
    # This is a testing comment
    app.run(host="0.0.0.0", port="8080")
