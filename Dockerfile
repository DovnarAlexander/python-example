FROM python:3.9.9-alpine3.15

WORKDIR /app
COPY python/ .
RUN pip install -r requirements.txt

ENTRYPOINT [ "python", "server.py" ]
